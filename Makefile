### -*-Makefile-*- to build and check the R package actuar
##
## Copyright (C) 2023 Vincent Goulet
##
## 'make build' builds the package.
##
## 'make check' checks the package.
##
## 'make check-as-cran' checks the package with the option '--as-cran'
##
## 'make all' is equivalent to 'make build check'
##
## Author: Vincent Goulet
##
## This file is part of actuar
## <https://gitlab.com/vigou3/actuar>.

## Package name
PKG = actuar

## Toolset
R = R CMD
Rbuild = ${R} build
Rcheck = ${R} check
Rcheck-as-cran = ${Rcheck} --as-cran

all: build check

.PHONY: build
build:
	${Rbuild} src

.PHONY: check
check:
	${Rcheck} $(shell ls -1t ${PKG}_*.tar.gz | head -n 1)

.PHONY: check-as-cran
check-as-cran:
	${Rcheck-as-cran} $(shell ls -1t ${PKG}_*.tar.gz | head -n 1)
